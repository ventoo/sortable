app = require('derby').createApp(module)
  .use(require 'derby-ui-boot')
  .use(require '../../ui/index.coffee')


# ROUTES #

app.get '/', (page, model, params, next) ->
  etc = model.at 'etc'

  etc.subscribe (err) ->
    return next err if err

    itemIds = etc.at 'itemIds'
    itemsQuery = model.query 'items', itemIds

    model.subscribe itemsQuery, (err) ->
      return next err if err

      model.refList '_page.items', 'items', itemIds, deleteRemoved: true
      page.render 'list'


# CONTROLLER FUNCTIONS #

app.fn 'list.add', (e, el) ->
  newItem = @model.del '_page.newItem'

  return unless newItem

  @model.push '_page.items', newItem

app.fn 'list.remove', (e, el) ->
  item = e.get ':item'
  items = @model.at '_page.items'
  index = items.get().indexOf item

  items.remove index

app.fn 'list.up', (e, el) ->
  item = e.get ':item'
  items = @model.at '_page.items'
  index = items.get().indexOf item

  return unless index

  items.move index, index - 1

app.fn 'list.down', (e, el) ->
  item = e.get ':item'
  items = @model.at '_page.items'
  index = items.get().indexOf item

  return if items.get().length - 1 is index

  items.move index, index + 1
